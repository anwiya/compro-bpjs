<?php

if( !session_id() ) session_start();

require_once '../app/init.php';

$app = new App;

?>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-VQDXJGZNLK"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-VQDXJGZNLK');
</script>
