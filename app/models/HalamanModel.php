<?php

class HalamanModel {
    private $table = 'halaman';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

    public function getAllHalaman () {
        $this->db->query('SELECT * FROM ' . $this->table);

        return $this->db->resultSet();
    }
    public function getHalamanById ($id) {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_halaman=:id');
        $this->db->bind('id', $id);

        return $this->db->single();
    }
    public function addHalaman($data) {
        $query = "INSERT INTO halaman (halaman) VALUES(:halaman)";
		$this->db->query($query);
		$this->db->bind('halaman',$data['halaman']);
		$this->db->execute();

		return $this->db->rowCount();
    }
    public function updateHalaman($data) {
        $query = "UPDATE halaman SET halaman=:halaman WHERE id_halaman=:id_halaman";
		$this->db->query($query);
		$this->db->bind('id_halaman',$data['id_halaman']);
		$this->db->bind('halaman',$data['halaman']);
		$this->db->execute();

		return $this->db->rowCount();
    }
    public function deleteHalaman($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE id_halaman=:id_halaman');
		$this->db->bind('id_halaman',$id);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function cariHalaman()
	{
		$key = $_POST['key'];
		$this->db->query("SELECT * FROM " . $this->table . " WHERE halaman LIKE :key");
		$this->db->bind('key',"%$key%");
		return $this->db->resultSet();
	}
}


?>