<?php

class KontenModel {
	
	private $table = 'konten';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllKonten()
	{
		$this->db->query("SELECT konten.*, section.section, halaman.halaman FROM " . $this->table . " JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section");
		return $this->db->resultSet();
	}

	public function getKontenById($id)
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_konten=:id');
		$this->db->bind('id',$id);
		return $this->db->single();
	}

	public function getSectionAndPage($id) 
	{
		if (empty($id)) {
			# code...
			$this->db->query('SELECT section.*, halaman.* FROM section JOIN halaman ON halaman.id_halaman = section.id_halaman');
	
			return $this->db->resultSet();
		}else{
			$this->db->query('SELECT section.*, halaman.* FROM section JOIN halaman ON halaman.id_halaman = section.id_halaman WHERE id_section=:id');
			$this->db->bind('id', $id);
	
			return $this->db->single();

		}
	}

	public function tambahKonten($data)
	{

		$dir = $_SERVER['DOCUMENT_ROOT'] . '/img/';
		$tmp = $_FILES['image']['tmp_name'];
		$filename = $_FILES['image']['name'];
		$error = $_FILES['image']['error'];

		if ($error) {
			Flasher::setMessage('Upload Gagal', 'Gagal', 'error');
		} else{
			move_uploaded_file($tmp, $dir.$filename);
		}

		$section = explode(",", $data['section_page']);


		$query = "INSERT INTO konten (id_section, id_halaman, head_title, content_title, content, image) VALUES(:id_section, :id_halaman, :head_title, :content_title, :content, :image)";
		$this->db->query($query);
		$this->db->bind('head_title', $data['head_title']);
		$this->db->bind('id_section', $section[1]);
		$this->db->bind('id_halaman', $section[0]);
		$this->db->bind('content_title', $data['content_title']);
		$this->db->bind('content', $data['content']);
		$this->db->bind('image', $filename);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function updateKonten($data)
	{
		$old_filename = $data['old_image'];
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/img/';
		$tmp = $_FILES['image']['tmp_name'];
		$filename;
		if ($_FILES['image']['name'] != null || $_FILES['image']['name'] != '') {
			$filename = $_FILES['image']['name'];
		}else{
			$filename = $data['old_image'];
		}
		$error = $_FILES['image']['error'];

		if ($error) {
			echo 'something went wrong';
			echo '<br />';
			echo $error;
		} else{
			if ($filename != $old_filename) {
				unlink($dir.$old_filename);
				move_uploaded_file($tmp, $dir.$filename);
			}
		}

		$section = explode(",", $data['section_page']);

		$query = "UPDATE konten SET id_section=:id_section, id_halaman=:id_halaman, head_title=:head_title, content_title=:content_title, content=:content, image=:image WHERE id_konten=:id";
		$this->db->query($query);
		$this->db->bind('id',$data['id_konten']);
		$this->db->bind('head_title', $data['head_title']);
		$this->db->bind('id_section', $section[1]);
		$this->db->bind('id_halaman', $section[0]);
		$this->db->bind('content_title', $data['content_title']);
		$this->db->bind('content', $data['content']);
		$this->db->bind('image', $filename);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function deleteKonten($id, $data)
	{
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/img/';
		$old_filename = $data['image'];
		$delete = unlink($dir.$old_filename);
		if ($delete > 0) {
			$this->db->query('DELETE FROM ' . $this->table . ' WHERE id_konten=:id');
			$this->db->bind('id',$id);
			$this->db->execute();
		} else{
			echo 'Something went wrong';
		}

		return $this->db->rowCount();
	}

	public function cariSection()
	{
		$key = $_POST['key'];
		// $this->db->query("SELECT * FROM " . $this->table . " WHERE section LIKE :key");
		$this->db->query("SELECT section.*, halaman.halaman FROM " . $this->table . " JOIN halaman ON halaman.id_halaman = section.id_halaman WHERE section LIKE :key");
		$this->db->bind('key',"%$key%");
		return $this->db->resultSet();
	}
}