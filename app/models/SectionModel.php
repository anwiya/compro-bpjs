<?php

class SectionModel {
	
	private $table = 'section';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllSection()
	{
		$this->db->query("SELECT section.*, halaman.halaman FROM " . $this->table . " JOIN halaman ON halaman.id_halaman = section.id_halaman");
		return $this->db->resultSet();
	}

	public function getSectionById($id)
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_section=:id');
		$this->db->bind('id',$id);
		return $this->db->single();
	}

	public function tambahSection($data)
	{
		$query = "INSERT INTO section (section, id_halaman) VALUES(:section, :id_halaman)";
		$this->db->query($query);
		$this->db->bind('section', $data['section']);
		$this->db->bind('id_halaman', $data['id_halaman']);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function updateSection($data)
	{
		$query = "UPDATE section SET section=:section, id_halaman=:id_halaman WHERE id_section=:id";
		$this->db->query($query);
		$this->db->bind('id',$data['id_section']);
		$this->db->bind('section', $data['section']);
		$this->db->bind('id_halaman', $data['id_halaman']);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function deleteSection($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE id_section=:id');
		$this->db->bind('id',$id);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function cariSection()
	{
		$key = $_POST['key'];
		// $this->db->query("SELECT * FROM " . $this->table . " WHERE section LIKE :key");
		$this->db->query("SELECT section.*, halaman.halaman FROM " . $this->table . " JOIN halaman ON halaman.id_halaman = section.id_halaman WHERE section LIKE :key");
		$this->db->bind('key',"%$key%");
		return $this->db->resultSet();
	}
}