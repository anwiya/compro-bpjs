<?php 

class Konten extends Controller {
    public function __construct()
    {
        if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
    }
    public function index()
	{
		$data['title'] = 'List Konten';
		$data['konten'] = $this->model('KontenModel')->getAllKonten();
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('konten/index', $data);
		$this->view('templates/footer');
	}
	public function cari()
	{
		$data['title'] = 'List Konten';
		$data['konten'] = $this->model('KontenModel')->cariKonten();
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('konten/index', $data);
		$this->view('templates/footer');
	}

	public function edit($id)
	{
		$data['title'] = 'Detail Konten';
		$data['konten'] = $this->model('KontenModel')->getKontenById($id);
		$data['listSection'] = $this->model('KontenModel')->getSectionAndPage('');
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('konten/edit', $data);
		$this->view('templates/footer');
	}

	public function tambah() 
	{
		$data['title'] = 'Tambah Konten';
        $data['listSection'] = $this->model('KontenModel')->getSectionAndPage('');	
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('konten/create', $data);
		$this->view('templates/footer');
	}

	public function simpanKonten()
	{		
		if( $this->model('KontenModel')->tambahKonten($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','ditambahkan','success');
			header('location: '. base_url . '/konten');
			exit;			
		}else{
			Flasher::setMessage('Gagal','ditambahkan','danger');
			header('location: '. base_url . '/konten');
			exit;	
		}
	}

	public function updateKonten(){	
		if( $this->model('KontenModel')->updateKonten($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','diupdate','success');
			header('location: '. base_url . '/konten');
			exit;			
		}else{
			Flasher::setMessage('Gagal','diupdate','danger');
			header('location: '. base_url . '/konten');
			exit;	
		}
	}

	public function hapus($id){
		$data['konten'] = $this->model('KontenModel')->getKontenById($id);
		if( $this->model('KontenModel')->deleteKonten($id, $data['konten']) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('location: '. base_url . '/konten');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('location: '. base_url . '/konten');
			exit;	
		}
	}
}