<?php 

class Halaman extends Controller {
    public function __construct()
    {
        if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
    }
    public function index()
	{
		$data['title'] = 'List Halaman';
		$data['halaman'] = $this->model('HalamanModel')->getAllHalaman();
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('halaman/index', $data);
		$this->view('templates/footer');
	}
	public function cari()
	{
		$data['title'] = 'List Halaman';
		$data['halaman'] = $this->model('HalamanModel')->cariHalaman();
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('halaman/index', $data);
		$this->view('templates/footer');
	}

	public function edit($id)
	{
		$data['title'] = 'Detail Halaman';
		$data['halaman'] = $this->model('HalamanModel')->getHalamanById($id);
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('halaman/edit', $data);
		$this->view('templates/footer');
	}

	public function tambah() 
	{
		$data['title'] = 'Tambah Halaman';		
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('halaman/create', $data);
		$this->view('templates/footer');
	}

	public function simpanHalaman()
	{		
		if( $this->model('HalamanModel')->addHalaman($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','ditambahkan','success');
			header('location: '. base_url . '/halaman');
			exit;			
		}else{
			Flasher::setMessage('Gagal','ditambahkan','danger');
			header('location: '. base_url . '/halaman');
			exit;	
		}
	}

	public function updateHalaman(){	
		if( $this->model('HalamanModel')->updateHalaman($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','diupdate','success');
			header('location: '. base_url . '/halaman');
			exit;			
		}else{
			Flasher::setMessage('Gagal','diupdate','danger');
			header('location: '. base_url . '/halaman');
			exit;	
		}
	}

	public function hapus($id){
		if( $this->model('HalamanModel')->deleteHalaman($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('location: '. base_url . '/halaman');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('location: '. base_url . '/halaman');
			exit;	
		}
	}
}