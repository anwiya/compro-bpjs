<?php 

class Section extends Controller {
    public function __construct()
    {
        if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
    }
    public function index()
	{
		$data['title'] = 'List Section Halaman';
		$data['section'] = $this->model('SectionModel')->getAllSection();
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('section/index', $data);
		$this->view('templates/footer');
	}
	public function cari()
	{
		$data['title'] = 'List Section Halaman';
		$data['section'] = $this->model('SectionModel')->cariSection();
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('section/index', $data);
		$this->view('templates/footer');
	}

	public function edit($id)
	{
		$data['title'] = 'Detail Section Halaman';
		$data['section'] = $this->model('SectionModel')->getSectionById($id);
        $data['listHalaman'] = $this->model('HalamanModel')->getAllHalaman();
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('section/edit', $data);
		$this->view('templates/footer');
	}

	public function tambah() 
	{
		$data['title'] = 'Tambah Section';
        $data['listHalaman'] = $this->model('HalamanModel')->getAllHalaman();		
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('section/create', $data);
		$this->view('templates/footer');
	}

	public function simpanSection()
	{		
		if( $this->model('SectionModel')->tambahSection($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','ditambahkan','success');
			header('location: '. base_url . '/section');
			exit;			
		}else{
			Flasher::setMessage('Gagal','ditambahkan','danger');
			header('location: '. base_url . '/section');
			exit;	
		}
	}

	public function updateSection(){	
		if( $this->model('SectionModel')->updateSection($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','diupdate','success');
			header('location: '. base_url . '/section');
			exit;			
		}else{
			Flasher::setMessage('Gagal','diupdate','danger');
			header('location: '. base_url . '/section');
			exit;	
		}
	}

	public function hapus($id){
		if( $this->model('SectionModel')->deleteSection($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('location: '. base_url . '/section');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('location: '. base_url . '/section');
			exit;	
		}
	}
}