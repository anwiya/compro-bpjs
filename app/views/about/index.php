  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $data['title'] ?></h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Kelompok | PRAKTEK KERJA INDUSTRI</h3>
        </div>
       <div class="card-body box-profile">
              <div class="d-flex justify-content-around">
                <div>
                  <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="https://cdn-icons-png.flaticon.com/512/3177/3177440.png" alt="User profile picture">
                  </div>
                  
                  <h3 class="profile-username text-center">Muhammad Ramzi</h3>
                
                  <p class="text-muted text-center">Web Developer</p>
                </div>
                <div class="pl-4">
                  <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="https://cdn-icons-png.flaticon.com/512/3177/3177440.png" alt="User profile picture">
                  </div>
                  
                  <h3 class="profile-username text-center">Mutiara Maulida</h3>
                
                  <p class="text-muted text-center">Web Developer</p>

                </div>
              </div>
              <ul class="list-group list-group-unbordered mb-3">
                <li class="mx-4 list-group-item">
                  <strong><i class="fas fa-map-marker-alt mr-1"></i></strong>
                  <a class="float-right"> SMK ADI SANGGORO</a>
                </li>
                <li class="mx-4 list-group-item">
                  <strong>

                    <p class="text-center"> XII RPL 2 </p>
                  </strong>
                </li> 
              </ul>
          </div>
        <!-- /.card-body --> 
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

