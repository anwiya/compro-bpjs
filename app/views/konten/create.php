  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Konten</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><?= $data['title']; ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?= base_url; ?>/konten/simpankonten" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label >untuk Section</label>
                    <select class="custom-select" name="section_page">
                      <option selected>Pilih Section</option>
                      <?php foreach ($data['listSection'] as $v) : ?>
                        <option value="<?= $v['id_halaman'] ?>, <?= $v['id_section'] ?>"><?= $v['halaman'] . ' - ' . $v['section'] ?></option>
                      <?php endforeach; ?>
                    </select>
                    <label >Head Title</label>
                    <input type="text" class="form-control" placeholder="masukkan heading..." name="head_title">
                    <label >Content Title</label>
                    <input type="text" class="form-control" placeholder="masukkan content title..." name="content_title">
                    <label >Content</label>
                    <input type="textarea" class="form-control" placeholder="tuliskan ..." name="content">
                    <label >Image</label>
                    <div>
                      <img src="#" id="img" alt="">
                    </div>
                    <input oninput="onPreview(event)" type="file" class="form-control" name="image" accept="image/*" />
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- NAMPILIN PREVIEW GAMBAR SEBELUM DI UPLOAD -->
  <script>
    function onPreview(event) {
      var output = document.getElementById('img');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = () => {
        URL.revokeObjectURL(output.src);
      }

    }
  </script>