  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Konten</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><?= $data['title']; ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?= base_url; ?>/konten/updatekonten" method="POST" enctype="multipart/form-data">

                <input type="hidden" name="id_konten" value="<?= $data['konten']['id_konten']; ?>">
                <input type="hidden" name="old_image" value="<?= $data['konten']['image']; ?>">
                <div class="card-body">
                  <div class="form-group">
                    <label >untuk Section</label>
                    <select class="custom-select" name="section_page">
                      <option selected>Pilih Section</option>
                      <?php foreach ($data['listSection'] as $v) : ?>
                        <?php if ($v['id_halaman'] == $data['konten']['id_halaman'] && $v['id_section'] == $data['konten']['id_section']) : ?>
                          <option value="<?= $v['id_halaman'] ?>, <?= $v['id_section'] ?>" selected><?= $v['halaman'] . ' - ' . $v['section'] ?></option>
                        <?php endif; ?>
                        <?php if ($v['id_halaman'] == $data['konten']['id_halaman'] && $v['id_section'] == $data['konten']['id_section']) : ?>
                          <option value="<?= $v['id_halaman'] ?>, <?= $v['id_section'] ?>"><?= $v['halaman'] . ' - ' . $v['section'] ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                    <label >Head Title</label>
                    <input type="text" class="form-control" placeholder="masukkan heading..." name="head_title" value="<?= $data['konten']['head_title'] ?>">
                    <label >Content Title</label>
                    <input type="text" class="form-control" placeholder="masukkan content title..." name="content_title" value="<?= $data['konten']['content_title'] ?>">
                    <label >Content</label>
                    <input type="textarea" class="form-control" placeholder="tuliskan ..." name="content" value="<?= $data['konten']['content'] ?>">
                    <label >Image</label>
                    <div class="d-flex border rounded">
                      <img src="<?= '../../../../../img/' . $data['konten']['image'] ?>" alt="">
                      <img id="img" src="#" alt="">
                    </div>
                    <input oninput="onPreview(event)" type="file" class="form-control" name="image" accept="image/*" />
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- NAMPILIN PREVIEW GAMBAR SEBELUM DI UPLOAD -->
  <script>
    function onPreview(event) {
      var output = document.getElementById('img');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = () => {
        URL.revokeObjectURL(output.src);
      }

    }
  </script>