  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Section</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><?= $data['title']; ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?= base_url; ?>/section/simpansection" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label >untuk Halaman</label>
                    <select class="custom-select" name="id_halaman">
                      <option selected>Pilih halaman</option>
                      <?php foreach ($data['listHalaman'] as $v) : ?>
                        <option value="<?= $v['id_halaman'] ?>"><?= $v['halaman'] ?></option>
                      <?php endforeach; ?>
                    </select>
                    <label >Section</label>
                    <input type="text" class="form-control" placeholder="masukkan section..." name="section">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->